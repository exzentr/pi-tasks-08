﻿#pragma once
using namespace std;
#include <iostream>
class Matrix
{
	int ColumnsNum;
	int RowsNum;
    double **matrix;
public:
	Matrix();
	Matrix(int rows, int  columns);
	Matrix(const Matrix& M);

	friend ostream& operator<<(ostream& stream, const Matrix& M);
	friend istream& operator>> (istream& stream, Matrix& M);

	Matrix& operator=(const Matrix& M);

	Matrix operator+(const Matrix& M);
	Matrix operator-(const Matrix& M);
	Matrix operator*(const Matrix& M);

	Matrix operator+(double num);
	Matrix operator-(double num);
	Matrix operator*(double num);
	Matrix operator/(double num);

    bool operator ==(const Matrix& M) const;
	bool operator !=(const Matrix& M) const;

	double* operator[](int index);

	double findDeterminant();
	Matrix CreatInverseMatrix();
	Matrix TranspositionMatrix();
	void CreatRandomMatrix();
	int DownloadFromFile();
	void AddNum(int num, int rows, int columns);
	int SaveToFile();
	~Matrix();
};

 ostream& operator<<(ostream& stream, const Matrix& M);
 istream& operator >> (istream& stream, Matrix& M);
﻿#include "Matrix.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");

	Matrix M1;
	M1.DownloadFromFile();
	cout << "М1: \n " << M1 << "\n" << endl;

	Matrix M2(4,4);
	M2.CreatRandomMatrix();
	cout << "М2: \n " << M2 << "\n" << endl;
	cout << "Умножение матриц\n" << M2*M1 <<"\n"<< endl;
	cout << "Сложение матриц\n" << M2+M1 << "\n" << endl;
	cout << "Вычитание матриц\n" << M2-M1 << "\n" << endl;

	cout <<"Матрица+число\n" << M1+5.0 << "\n" << endl;
	cout << "Матрица-число\n" << M1-5.0 << "\n" << endl;
	cout << "Матрица*число\n" << M1*5.0 << "\n" << endl;
	cout << "Матрица/число \n" << M1*5.0 << "\n" << endl;

	if (M1 == M2)
		cout << "Матрицы М1 и М2 равны" << endl;
	else
		cout << "Матрицы М1 и М2  не равны" << endl;
	
	M1[1][0] = 6;
	cout << M1[1][0];

	cout << "Определитель M1 :" << M1.findDeterminant() << endl;
	cout << "Транспонированная М1 \n" << M1.TranspositionMatrix() << "\n" << endl;
	cout << "Обратная М1 \n" << M1.CreatInverseMatrix() << "\n" << endl;

	M1.SaveToFile();
	return 0;
}
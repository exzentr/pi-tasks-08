﻿#include "Matrix.h"
#include <time.h>
#include <fstream>

Matrix::Matrix()
{
	RowsNum = 0;
	ColumnsNum = 0;
	matrix = nullptr;
}
Matrix::Matrix(int rows, int  columns)
{
	RowsNum = rows;
	ColumnsNum = columns;

	matrix = new double*[rows];
	for (int i = 0; i < rows; i++)
	{
		matrix[i] = new double[columns];

		for (int j = 0; j < columns; j++)
			matrix[i][j] = 0.0;
	}
}
Matrix::Matrix(const Matrix & M)
{
	RowsNum = M.RowsNum;
	ColumnsNum = M.ColumnsNum;
	matrix = new double*[RowsNum];
	for (int i = 0; i < RowsNum; i++)
	{
		matrix[i] = new double[ColumnsNum];
		matrix[i] = M.matrix[i];
	}

}

Matrix & Matrix::operator=(const Matrix &M)
{
	if (this != &M)
	{
		RowsNum = M.RowsNum;
		ColumnsNum = M.ColumnsNum;
		matrix = new double*[RowsNum];
		for (int i = 0; i < RowsNum; i++)
			matrix[i] = M.matrix[i];
	}
	return *this;
}
Matrix Matrix::operator+(const Matrix & M)
{
	if (ColumnsNum != M.ColumnsNum || RowsNum != M.RowsNum)
	{
		cout << "Ошибка! Матрицы разных порядков!" << endl;
		return Matrix();
	}
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j] + M.matrix[i][j];
	}
		return tmp;
}
Matrix Matrix::operator-(const Matrix & M)
{
	if (ColumnsNum != M.ColumnsNum || RowsNum != M.RowsNum)
	{
		cout << "Ошибка! Матрицы разных порядков!" << endl;
		return Matrix();
	}
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j] - M.matrix[i][j];
	}
	return tmp;
}
Matrix Matrix::operator*(const Matrix & M)
{
	   if (ColumnsNum != M.RowsNum)
	     {
		   cout << "Ошибка! Число столбцов не равно числу строк" << endl;
		   return Matrix();
	     }
	   Matrix tmp(RowsNum, M.ColumnsNum);
	for (int i = 0; i < tmp.RowsNum; i++)
		for (int j = 0; j < tmp.ColumnsNum; j++)
			for (int k = 0; k < ColumnsNum; k++)
				tmp.matrix[i][j] += matrix[i][k] * M.matrix[k][j];
	return tmp;
}

Matrix Matrix::operator+(double num)
{
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j]+num;
	}
	return tmp;
}
Matrix Matrix::operator-(double num)
{
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j] - num;
	}
	return tmp;
}
Matrix Matrix::operator*(double num)
{
	if (num==0)
	{
		cout << "Ошибка! Зачем нужно умножать на ноль?!" << endl;
		return Matrix();
	}
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j] * num;
	}
	return tmp;
}
Matrix Matrix::operator/(double num)
{
	if (num == 0)
	{
		cout << "Ошибка! Нельзя делить на ноль!!!" << endl;
		return Matrix();
	}
	Matrix tmp(RowsNum, ColumnsNum);
	for (int i = 0; i < RowsNum; i++)
	{
		for (int j = 0; j < ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[i][j] / num;
	}
	return tmp;
}

bool Matrix::operator==(const Matrix & M) const
{
	for (int i = 0; i < RowsNum; i++)
		for (int j = 0; j < ColumnsNum; j++)
			if (matrix[i][j] != M.matrix[i][j])
				return false;
	return true;
}
bool Matrix::operator!=(const Matrix & M) const
{
	for (int i = 0; i < RowsNum; i++)
		for (int j = 0; j < ColumnsNum; j++)
			if (matrix[i][j] == M.matrix[i][j])
				return false;
	return true;
}

double * Matrix::operator[](int index)
{
	return matrix[index];
}

double Matrix::findDeterminant()
{
	int i = 0; //всегра раскладываем по первой строке в матрицк, для удобства
	if (RowsNum != ColumnsNum)
	{
		cout << "Матрица не квадратная! Определитель посчитать нельзя" << endl;
		exit(EXIT_FAILURE);
	}
	if (RowsNum == 1 && ColumnsNum == 1)
		return matrix[0][0];
	if (RowsNum == 2 && ColumnsNum == 2)
		return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
	int n; double det = 0;
	for (int j = 0; j < ColumnsNum; j++)
	{
		if ((i+1 + j+1) % 2 == 0) n = 1;
		else n = -1;
		Matrix tmp((this->RowsNum - 1),( this->ColumnsNum - 1));
		for (int k = 0; k < this->RowsNum; k++)
		{
			if (k == i)	continue;
			for (int m = 0; m < this->ColumnsNum; m++)
			{
				if (m == j) continue;
				if (k > i)
					if (m > j)
						 tmp.matrix[k - 1][m - 1] = matrix[k][m];
					else tmp.matrix[k - 1][m] = matrix[k][m];
				else 
					if (m > j)
						tmp.matrix[k][m - 1] = matrix[k][m];
					else tmp.matrix[k][m] = matrix[k][m];
			}
		}
		det += matrix[i][j] * n * tmp.findDeterminant();
	}
	return det;
}
Matrix Matrix::CreatInverseMatrix()
{
	int det = findDeterminant();
	if (det == 0)
	{
		cout << "Матрица не обратима" << endl;
		return Matrix();
	}
	Matrix InMat(RowsNum, ColumnsNum);
	int n;
	for (int i = 0; i < RowsNum; i++)
		for (int j = 0; j < ColumnsNum; j++)
		{
			if ((i + 1 + j + 1) % 2 == 0) n = 1;
			else n = -1;
			/*создаем матрицу с "вычеркнутыми" i и j*/
			Matrix tmp((RowsNum - 1), (ColumnsNum - 1));
			for (int k = 0; k < RowsNum; k++)
			{
				if (k == i)	continue;
				for (int m = 0; m < ColumnsNum; m++)
				{
					if (m == j) continue;
					if (k > i)
						if (m > j)
							tmp.matrix[k - 1][m - 1] = matrix[k][m];
						else tmp.matrix[k - 1][m] = matrix[k][m];
					else
						if (m > j)
							tmp.matrix[k][m - 1] = matrix[k][m];
						else tmp.matrix[k][m] = matrix[k][m];
				}
			}
			int dfgk = n*tmp.findDeterminant();
			InMat.matrix[i][j] = n*tmp.findDeterminant();
		}
	InMat.TranspositionMatrix();
	for (int i = 0; i < RowsNum; i++)
		for (int j = 0; j < ColumnsNum; j++)
			InMat.matrix[i][j] /= det;
	return InMat;
}
Matrix Matrix::TranspositionMatrix()
{
	Matrix tmp(ColumnsNum, RowsNum);
	for (int i = 0; i < tmp.RowsNum; i++)
		for (int j = 0; j < tmp.ColumnsNum; j++)
			tmp.matrix[i][j] = matrix[j][i];
	return tmp;
}
void Matrix::CreatRandomMatrix()
{
	srand(time(NULL));
	if (RowsNum == 0 && ColumnsNum == 0)
	{
		RowsNum = rand() % 20 + 3;
		ColumnsNum = rand() % 20 + 3;
		matrix = new double*[RowsNum];
		for (int i = 0; i < RowsNum; i++)
		{
			matrix[i] = new double[ColumnsNum];

			for (int j = 0; j < ColumnsNum; j++)
				matrix[i][j] = 0.0;
		}
		
	}
	for (int i = 0; i < RowsNum; i++)
		for (int j = 0; j < ColumnsNum; j++)
			matrix[i][j] = rand() % 100;
}
int Matrix::DownloadFromFile()
{
	ifstream file("matrix.txt");
	if (!file.is_open()) // если файл небыл открыт
	{
		cout << "Файл matrix.txt не может быть открыт\n";
		return 1;
	}
	char  str[256];
	int curRows = -1, curCol = -1;
	while (!file.eof())
	{
		curRows++;
		curCol = -1;
		int tempNum = 0; int i = 0;
		file.getline(str, 256);
		while (str[i]!='\0')
		{
			if (str[i] >= '0' && str[i] <= '9')
			{
				tempNum= tempNum * 10 + str[i] - '0';
				if (str[i + 1] == '\0')
				{
					curCol++;
					AddNum(tempNum, curRows, curCol);
					tempNum = 0;
				}
				i++;
			}
			else
			{
				curCol++;
				i++;
				AddNum(tempNum, curRows, curCol);
				tempNum = 0;
			}
		}
	}
}
void Matrix::AddNum(int num, int rows, int columns)
{
	Matrix tmp(*this);
	if (ColumnsNum < columns + 1)
		ColumnsNum = columns + 1;
	if (RowsNum < rows + 1)
		RowsNum = rows + 1;

	matrix = new double*[RowsNum];
	for (int i = 0; i < RowsNum; i++)
		matrix[i] = new double[ColumnsNum];

	for (int i = 0; i < tmp.RowsNum; i++)
		for (int j = 0; j < tmp.ColumnsNum; j++)
			matrix[i][j] = tmp.matrix[i][j];

	matrix[rows][columns] = num;
}
int Matrix::SaveToFile()
{
	ofstream file("matrixOut.txt");
	if (!file.is_open()) // если файл небыл открыт
	{
		cout << "Файл matrix.txt не может быть открыт\n";
		return 1;
	}
	file << "Матрица:\n" << endl;
	file << *this << endl;
	file << "\n Определитель: " << fixed << findDeterminant() << endl;
	file << "\nТранспонированная матрица:\n\n" << TranspositionMatrix() << endl;
	file << "\nОбратная матрица:\n\n" << CreatInverseMatrix() << endl;
	file.close();
	return 0;
}

Matrix::~Matrix()
{
	
}

ostream & operator<<(ostream & stream, const Matrix & M)
{
	for (int i = 0; i < M.RowsNum; i++)
	{
		for (int j = 0; j < M.ColumnsNum; j++)
			stream << M.matrix[i][j] << " ";
		stream << endl;
	}

	return stream;
}
istream & operator >> (istream & stream, Matrix & M)
{
	cout << "введите число строк: "; stream >> M.RowsNum;
	cout << "Введите число столбцов: "; stream >> M.ColumnsNum;
	cout << endl;
	for (int i = 0; i < M.RowsNum; i++)
		for (int j = 0; j< M.ColumnsNum; j++)
			stream >> M.matrix[i][j];
	return stream;
}

